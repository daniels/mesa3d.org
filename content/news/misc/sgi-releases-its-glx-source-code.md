---
title:    "SGI releases its GLX source code"
date:     1999-02-16 00:00:00
category: misc
tags:     []
---
[SGI](https://www.sgi.com/) releases its [GLX source
code](http://web.archive.org/web/20040805154836/http://www.sgi.com/software/opensource/glx/download.html).
