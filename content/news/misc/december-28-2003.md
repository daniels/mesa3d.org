---
title:    "December 28, 2003"
date:     2003-12-28 00:00:00
category: misc
tags:     []
---
The Mesa CVS server has been moved to
[freedesktop.org](https://www.freedesktop.org) because of problems with
SourceForge's anonymous CVS service.
