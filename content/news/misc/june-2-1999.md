---
title:    "June 2, 1999"
date:     1999-06-02 00:00:00
category: misc
tags:     []
---
[nVidia](https://www.nvidia.com/) has released some Linux binaries for
xfree86 3.3.3.1, along with the **full source**, which includes GLX
acceleration based on Mesa 3.0. They can be downloaded from
<https://www.nvidia.com/Products.nsf/htmlmedia/software_drivers.html>.
