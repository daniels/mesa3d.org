---
title:    "August 17, 1999"
date:     1999-08-17 00:00:00
category: misc
tags:     []
summary:  "A report from the SIGGRAPH '99 Linux/OpenGL BOF meeting is now
available."
---
A report from the SIGGRAPH '99 Linux/OpenGL BOF meeting is now
available.

\-Brian
