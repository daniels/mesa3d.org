---
title:    "Mesa 19.3.4 is released"
date:     2020-02-13 00:00:00
category: releases
tags:     []
---
[Mesa 19.3.4](/relnotes/19.3.4.html) is released. This is a bug fix
release.
